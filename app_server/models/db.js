
var mongoose = require('mongoose');

//var dbURI = 'mongodb://localhost/loc8r';
var dbURI = 'mongodb://heroku_lm8f9nzr:uigpgtbfd53ii2eh8jpu5bl6g4@ds231133.mlab.com:31133/heroku_lm8f9nzr'

mongoose.connect(dbURI);



mongoose.connection.on('connected', () => {
    console.log('Mongoose connected to ' + dbURI);
});

mongoose.connection.on('error', err => {
    console.log('Mongoose connected error', err);
});

mongoose.connection.on('disconnected', () => {
    console.log('Mongoose disconnected');
});



const gracefulShutdown = (msg, callback) => {
    mongoose.connection.close( () => {
        console.log('Mongoose disconnected through ${msg}');
        callback();
    });
};


process.once('SIGUSR2', () => {
    gracefulShutdown('nodemon restart', () =>{
        process.kill(process.pid, 'SIGUSR2');
    });
});

process.once('SIGINT', () => {
    gracefulShutdown('app termination', () =>{
        process.exit(0);
    });
});

process.once('SIGTERM', () => {
    gracefulShutdown('Heroku app shutdown', () =>{
        process.exit(0);
    });
});