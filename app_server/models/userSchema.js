const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema =  new Schema({
    username: String,
    password: String,
    log: [{type: String}]
   }
);

module.exports = mongoose.model('User', UserSchema);