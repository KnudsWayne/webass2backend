var jwt = require('jsonwebtoken');

function verifyToken(req,res,next){

    var token = req.headers['token'];
    //console.log("Hello from verifyer. Token: " + req.headers['token']);
    
    if(!token){
        return res.status(403).send({message : "No token", auth:false});
    }

    jwt.verify(token, 'secret', function(err,user)
    {
      if(err){
      return res.status(500).send({message: err, auth:false});
      }
      
      next();            
    })
}

module.exports = verifyToken;