var express = require('express');
var router = express.Router();
var verifyToken = require('../verifyToken/verifyToken');


// Require our controllers.
var workoutplan_controller = require('../controllers/workoutplanController'); 
var exercise_controller = require('../controllers/exerciseController');
var user_controller = require('../controllers/userController');

/// WORKOUTPLAN ROUTES ///

// GET catalog home page.
router.get('/', workoutplan_controller.index);  

// GET request for creating a Workoutplan. NOTE This must come before routes that display Workoutplan (uses id).
router.get('/workoutplan/create',workoutplan_controller.workoutplan_create_get);



router.post('/workoutplan/create', verifyToken,workoutplan_controller.workoutplan_create_post);

router.get('/workoutplan/:id', workoutplan_controller.workoutplan_detail)

router.get('/workoutplan/:id/update', workoutplan_controller.workoutplan_update_get)

router.post('/workoutplan/:id/update', verifyToken,workoutplan_controller.workoutplan_update_post)

router.get('/workoutplan', workoutplan_controller.workoutplan_list);

/// EXERCISE ROUTES ///

// GET request for creating Exercise. NOTE This must come before route for id (i.e. display exercise).  
router.get('/exercise/create', exercise_controller.exercise_create_get);

// POST request for creating Exercise
router.post('/exercise/create', verifyToken, exercise_controller.exercise_create_post);

router.get('/exercise', exercise_controller.exercise_list);


/// USER ROUTES ///
router.get('/signup', user_controller.user_signup_get);

router.post('/signup', user_controller.user_signup_post);

router.get('/login', user_controller.user_login_get);

router.post('/login', user_controller.user_login_post);

router.get('/user/:id', user_controller.user_get);

router.post('/user/:id/update', user_controller.user_update_post);

router.get('/logout',user_controller.user_logut);

module.exports = router;