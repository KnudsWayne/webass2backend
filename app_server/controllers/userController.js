var mongoose = require('mongoose');
var User = require('../models/userSchema');
var jwt = require('jsonwebtoken');


exports.user_login_get = function (req, res) {
    res.status(401).json({ message: "logging in" })
}

exports.user_login_post = function (req, res) {

    if (!req.body.username || !req.body.password) {
        return res.status(400).json({ message: "missing inputs" });
    }

    User.findOne({
        username: req.body.username,
    }, function (err, user) {

        if (err) return res.status(500).send('Error');

        if (!user) {
            return res.status(401).json({ message: "User Not Found" })
        }
        if (user.password != req.body.password) {
            return res.status(401).send({ auth: false, token: null })
        }
        else {
            var token = jwt.sign({ id: user._id, username: user.username }, 'secret', { expiresIn: 43200 });
            res.status(200).send({ auth: true, token: token, username: user.username });
        }
    })
}

exports.user_signup_get = function (req, res) {
    res.status(401).json({ message: "signing up" })
}

exports.user_signup_post = function (req, res) {

    User.findOne({
        username: req.body.username
    }, function (err, user) {
        if (err) return res.status(500).send('Error');
        if (!user) {
            //user not found creating new
            var newUser = new User({
                username: req.body.username,
                password: req.body.password,
                log: []
            })

            newUser.save(function (err) {
                if (err) { return next(err); }
            })
            res.status(201).json(201);
        }
        else {
            res.status(401).json({ message: "User already exists" })
        }
    })
}


//TODO add user_update_post
exports.user_update_post = function(req, res, next){

    // Convert the log to an array.
    if(!(req.body.log instanceof Array)){
        if(typeof req.body.log ==='undefined')
            req.body.log = [{type: String}];
        else
            req.body.log = new Array(req.body.log);
    }
    

    var updatedUser = new User({
        _id: req.params.id,
        username: req.body.username,
        password : req.body.password,
        log: req.body.log
    });

    User.findByIdAndUpdate(req.params.id, updatedUser, {}, function(err, theUpdatedUser){
        if(err) {return next(err);}
        res.status(200).json(200);
    })

}

exports.user_get = function(req, res, next)
{
    User.findById(req.params.id)
    .exec(function(err, results)    {
        res.status(200).json(results)
    });
}



exports.user_logut = function (req, res) {
    res.status(200).send({ auth: false, token: null });
}

exports.loginRequired = function (req, res, next) {
    if (req.user) {
        next();
    } else {
        return res.status(401).json({ message: 'unauthorized user!' });
    }
}









