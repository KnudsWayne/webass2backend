
var Exercise = require('../models/exerciseSchema')

exports.exercise_create_get = function(req, res) {
    res.render('exercise_form', { title: 'Create new Exercise' });
};

exports.exercise_create_post = function(req, res, next) {

    var newExercise = new Exercise({
        name: req.body.name,
        description: req.body.description,
        sets: req.body.sets,
        reps: req.body.reps,
    })


    newExercise.save(function(err){
        if(err){return next(err);}

        //res.redirect("/exercise/create")
        //saved!
        res.status(201).json(201);
    })
};


// Display list of all workoutplans.
exports.exercise_list = function(req, res) {

    Exercise.find({}).exec(function (err, result) {
        if (err) { return next(err); }
        
        // Successful, so render
        //res.render('exercise_list', { title: 'All Exercises', exercise_list:  result});

        res.status(200).json(result);
      });
  
  };