var WorkoutPlan = require("../models/workoutplanSchema");
var Exercise = require("../models/exerciseSchema");
var async = require('async');
//var VerifyToken = require('../verifyToken/verifyToken');


exports.index = function(req, res, next){
    res.render('index', {title: 'Hello there!'});
  }

exports.workoutplan_create_get = function(req, res) {
   
    Exercise.find({}).exec(function(err, results)    {
        res.status(200).json(results)
    });
}

// exports.exercise_create_get = function(req, res) {
//     res.render('workoutplan_list', { title: 'All workoutplans' });
// };


exports.workoutplan_create_post = function(req, res, next) {
    
    
     // Convert the exercises to an array.
    if(!(req.body.exercises instanceof Array)){
        if(typeof req.body.exercises==='undefined')
        req.body.exercises=[];
        else
        req.body.exercises=new Array(req.body.exercises);
    }    

    var newWorkoutPlan = new WorkoutPlan({
        name: req.body.name,
        exercises : req.body.exercises
    })
    
    console.log("tjekket nok");
    newWorkoutPlan.save(function(err){
        if(err){return res.status(401).json({message : "couldnt save the workoutplan"})}        
        //saved!
        res.status(201).send({message : "Workoutplan added"});
    })
};

exports.workoutplan_detail = function(req, res, next)
{
    WorkoutPlan.findById(req.params.id)
    .populate('exercises')
    .exec(function(err, results)    {
        res.status(200).json(results)
    });
}

//UPDATE
exports.workoutplan_update_get = function(req, res){
    
    async.parallel({
        workoutplan: function(callback) {
            WorkoutPlan.findById(req.params.id).populate('exercises').exec(callback);
        },
        exercises: function(callback) {
            Exercise.find(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            if (results.workoutplan==null) { // No results.
                var err = new Error('Workoutplan not found');
                err.status = 404;
                return next(err);
            }
            // Success.
            // Mark our selected genres as checked.
            for (var all_ex_iter = 0; all_ex_iter < results.exercises.length; all_ex_iter++) {
                for (var WOP_ex_iter = 0; WOP_ex_iter < results.workoutplan.exercises.length; WOP_ex_iter++) {
                    if (results.exercises[all_ex_iter]._id.toString()==results.workoutplan.exercises[WOP_ex_iter]._id.toString()) {
                        results.exercises[all_ex_iter].checked='true';
                    }
                }
            }

            //***MANGLER RETURNER FLERE TING***
            //res.render('workoutplan_form', { title: 'Update workoutplan', exercises:results.exercises, workoutplan: results.workoutplan });
            res.status(200).json(results);
        });
}

exports.workoutplan_update_post = function(req, res, next){

        // Convert the exercises to an array.
        if(!(req.body.exercises instanceof Array)){
            if(typeof req.body.exercises==='undefined')
            req.body.exercises=[];
            else
            req.body.exercises=new Array(req.body.exercises);
        }
        
    
        var updatedWorkoutPlan = new WorkoutPlan({
            _id: req.params.id,
            name: req.body.name,
            exercises : req.body.exercises
        });

        WorkoutPlan.findByIdAndUpdate(req.params.id, updatedWorkoutPlan, {}, function(err, theUpdatedWop){
            if(err) {return next(err);}
            res.status(200).json(200);
        })

}

// Display list of all workoutplans.
exports.workoutplan_list = function(req, res) {

    WorkoutPlan.find({}).exec(function (err, result) {
        if (err) { return next(err); }
        // Successful, so render
        res.status(200).json(result);
        
      });
  
  };
